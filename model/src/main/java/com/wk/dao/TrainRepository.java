package com.wk.dao;

import com.wk.model.Carriage;
import com.wk.model.Train;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TrainRepository extends JpaRepository<Train, Long> {
    @EntityGraph(attributePaths = {"carriages"})
    Optional<Train> findByTrainNumber(@Param("trainNumber") String trainNumber);

    @Query("select t from Train t where t.trainNumber = :trainNumber")
    Optional<Train> findTrainByTrainNumber(@Param("trainNumber") String trainNumber);

    @Query("select case when (count(t)>0) then true else false end from Train t where t.trainNumber = :trainNumber")
    boolean testTrainByTrainNumber(@Param("trainNumber") String trainNumber);

    @Query("select distinct t from Train t left join fetch t.carriages c order by t.trainNumber")
    List<Train> findAllByOneSelect();

    @Query("select case when (count(t)>0) then true else false end from Train t left join t.carriages c where t.trainNumber = :trainNumber and c.carriageNumber = :carriageNumber")
    boolean testCarriageNumber(@Param("trainNumber") String trainNumber, @Param("carriageNumber") String carriageNumber);

    @Query("select c from Train t left join t.carriages c where  t.trainNumber = :trainNumber and c.carriageNumber = :carriageNumber")
    Optional<Carriage> findByTrainNumberAndCarriageNumber(@Param("trainNumber") String trainNumber, @Param("carriageNumber") String carriageNumber);
}
