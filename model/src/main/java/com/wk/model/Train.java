package com.wk.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="train", uniqueConstraints = {
        @UniqueConstraint(columnNames = "trainNumber")
})
public class Train {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;
    private String trainNumber;
    @OneToMany(mappedBy = "train")
    private Set<Carriage> carriages = new HashSet<>();

    private Train() {
    }

    public Train(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Long getId() {
        return id;
    }

    public Set<Carriage> getCarriages() {
        return carriages;
    }

    public void setCarriages(Set<Carriage> carriages) {
        this.carriages = carriages;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }
}
