package com.wk.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name="carriage")
public class Carriage {
    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    private String carriageNumber;
    @JsonIgnore
    @ManyToOne
    private Train train;

    private Carriage() {}

    public Carriage(final Train train, String carriageNumber) {
        this.carriageNumber = carriageNumber;
        this.train = train;
    }

    public Long getId() {
        return id;
    }

    public String getCarriageNumber() {
        return carriageNumber;
    }

    public void setCarriageNumber(String carriageNumber) {
        this.carriageNumber = carriageNumber;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }
}
