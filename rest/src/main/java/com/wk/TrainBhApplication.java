package com.wk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

// same as @Configuration @EnableAutoConfiguration @ComponentScan
@SpringBootApplication(scanBasePackages = {"com.wk", "com.wk.service"})
@EnableJpaRepositories(basePackages = "com.wk.dao")
@EntityScan("com.wk.model")
@EnableTransactionManagement
public class TrainBhApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainBhApplication.class, args);
    }
}
