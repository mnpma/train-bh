package com.wk.web;

import com.wk.dto.CarriageDto;
import com.wk.dto.TrainDto;
import com.wk.service.TrainService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping
@Api(value="onlineinfo", description="Operations pertaining to train in Online Information")
public class TrainController {
    @Autowired
    private TrainService trainService;

    @ApiOperation(value = "Initializes the database with above data {trains: 278, 938}, returns 204 No Content")
    @ApiResponse(code=204, message = "")
    @RequestMapping(path = "/init", method = RequestMethod.GET)
    public ResponseEntity<?> init() {
        trainService.init();
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value="Returns all available trains")
    @ApiResponses(value= {
            @ApiResponse(code=200, message = "Successfully retrieved available trains", response = TrainDto.class, responseContainer = "List"),
            @ApiResponse(code=204, message = "no content")
    }
    )
    @RequestMapping(path = "/train", method = RequestMethod.GET)
    public ResponseEntity<List<TrainDto>> getTrains() {
        List<TrainDto> trainDtoList = trainService.getTrains();
        return trainDtoList == null ? ResponseEntity.noContent().build() : ResponseEntity.ok(trainDtoList);
    }

    @ApiOperation(value="Returns a particular train", response = TrainDto.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message = "Successfully retrieved available trains"),
            @ApiResponse(code=404, message = "not found")
    }
    )
    @RequestMapping(path = "/train/{id}", method = RequestMethod.GET)
    public ResponseEntity<TrainDto> getTrain(
            @ApiParam(value = "ID of train that needs to be fetched", allowableValues = "range[1,10000]", required = true)
            @PathVariable(name = "id") String trainNumber) {
        return ResponseEntity.ok(trainService.getTrain(trainNumber));
    }

    @ApiOperation(value="Creates a new train  given body and returns newly created train.", response = TrainDto.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message = "returns newly created train"),
            @ApiResponse(code=400, message = "train number already exist on another train")
    }
    )
    @RequestMapping(path = "/train", method = RequestMethod.POST)
    public ResponseEntity<TrainDto> addTrain(@RequestBody TrainDto input) {
        return ResponseEntity.ok(trainService.addTrain(input));
    }

    @ApiOperation(value="Modify the train by adding a carriage to it and returning the resulting train..", response = TrainDto.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message = "returns updating train"),
            @ApiResponse(code=400, message = "bad request, for example the carriage number already exist on the particular train."),
            @ApiResponse(code=404, message = "train not found")
    }
    )
    @RequestMapping(path="/train/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TrainDto> updateTrainAddCarriage(@PathVariable(name = "id") String trainNumber,
                             @RequestBody CarriageDto input) {
        return ResponseEntity.ok(trainService.updateTrainAddCarriage(trainNumber, input));
    }

    @ApiOperation(value="Remove carriage with {carriageId} from train and return the resulting train.", response = TrainDto.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message = "returns updated train"),
            @ApiResponse(code=404, message = "carriage not found")
    }
    )
    @RequestMapping(path="/train/{id}/carriage/{carriageId}", method = RequestMethod.DELETE)
    public ResponseEntity<TrainDto> updateTrainRemoveCarriage(@PathVariable(name = "id") String trainNumber,
                                           @PathVariable(name="carriageId") String carriageNumber) {
        return ResponseEntity.ok(trainService.updateTrainRemoveCarriage(trainNumber, carriageNumber));
    }

}
