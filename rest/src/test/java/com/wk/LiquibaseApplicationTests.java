package com.wk;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.core.NestedCheckedException;

import java.net.ConnectException;

import static org.assertj.core.api.Assertions.assertThat;

public class LiquibaseApplicationTests {

    @Rule
    public OutputCapture outputCapture = new OutputCapture();

    @Test
    public void testDefaultSettings() throws Exception {
        try {
            TrainBhApplication.main(new String[] { "--server.port=0" });
        }
        catch (IllegalStateException ex) {
            if (serverNotRunning(ex)) {
                return;
            }
        }
        String output = this.outputCapture.toString();
        //db/changelog/changesets/init-db.xml
        //ChangeSet db/changelog/changesets/init-db.xml::21-02-2018-2::mnpma ran successfully
        assertThat(output).contains("Successfully acquired change log lock")
                .contains("Creating database history "
                        + "table with name: PUBLIC.DATABASECHANGELOG")
                .contains("Table train created")
                .contains("ChangeSet db/"
                        + "changelog/changesets/init-db.xml::21-02-2018-1::"
                        + "mnpma ran successfully")
                .contains("Table carriage created")
                .contains("ChangeSet db/changelog/changesets/"
                        + "init-db.xml::21-02-2018-2::"
                        + "mnpma ran successfully")
                .contains("Foreign key constraint added to carriage (train_id)")
                .contains("Successfully released change log lock");
    }

    @SuppressWarnings("serial")
    private boolean serverNotRunning(IllegalStateException ex) {
        NestedCheckedException nested = new NestedCheckedException("failed", ex) {
        };
        if (nested.contains(ConnectException.class)) {
            Throwable root = nested.getRootCause();
            if (root.getMessage().contains("Connection refused")) {
                return true;
            }
        }
        return false;
    }}
