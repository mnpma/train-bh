package com.wk.web;

import com.wk.TrainBhApplication;
import com.wk.dto.CarriageDto;
import com.wk.dto.TrainDto;
import com.wk.model.Carriage;
import com.wk.model.Train;
import com.wk.service.CarriageService;
import com.wk.service.TrainService;
import org.junit.Before;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrainBhApplication.class)
@WebAppConfiguration
public class TrainControllerTest {
    private static final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private TrainDto train;
    private List<CarriageDto> carriageList = new ArrayList<>();
    @Autowired
    private TrainService trainService;
    @Autowired
    private CarriageService carriageService;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        this.carriageService.deleteAllInBatch();
        this.trainService.deleteAllInBatch();

        this.train = trainService.save(new Train("222"));
        this.carriageList.add(carriageService.save(new Carriage(null, "1")));
        this.carriageList.add(carriageService.save(new Carriage(null, "2")));
    }

    @Test
    public void trainNotFound() throws Exception {
        mockMvc.perform(get("/train/112"))
                .andExpect(status().isNotFound());
    }


    @Test
    public void carriageNotFound() throws Exception {
        mockMvc.perform(delete("/train/222/carriage/3"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getTrains() throws Exception {
        mockMvc.perform(get("/train"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void getTrainByTrainNumber() throws Exception {
        mockMvc.perform(get("/train/222"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.trainNumber", is(this.train.getTrainNumber())));
    }

    @Test
    public void updateTrainAddCarriages() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/train/222").
                content(this.json(new CarriageDto("3"))
                ).contentType(contentType))
                .andExpect(status().isOk());
    }

    @Test
    public void updateTrainAddCarriagesExpectedException() throws Exception {
        TrainDto newTrain = new TrainDto("333");
        CarriageDto c = new CarriageDto("1");
        newTrain.getCarriages().add(c);
        c= new CarriageDto("2");
        newTrain.getCarriages().add(c);
        mockMvc.perform(post("/train")
                .contentType(contentType)
                .content(this.json(newTrain))
        ).andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.put("/train/333").
                content(this.json(new CarriageDto( "1"))
                ).contentType(contentType))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void addTrainAndCarriages() throws Exception {
        TrainDto newTrain = new TrainDto("333");
        CarriageDto c = new CarriageDto("1");
        newTrain.getCarriages().add(c);
        c= new CarriageDto("2");
        newTrain.getCarriages().add(c);
        mockMvc.perform(post("/train")
                .contentType(contentType)
                .content(this.json(newTrain))
        ).andExpect(status().isOk());
    }

    @Test
    public void addTrainAndCarriagesExpectedException() throws Exception {
        mockMvc.perform(post("/train")
                .contentType(contentType)
                .content(this.json(this.train))
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void initTest() throws Exception {
        mockMvc.perform(get("/init"))
                .andExpect(status().isNoContent());
        TrainDto train = trainService.getTrain("278");
        assertNotNull(train);
        assertTrue("278".equals(train.getTrainNumber()));
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
