package com.wk.service;

import com.wk.dao.CarriageRepository;
import com.wk.dao.TrainRepository;
import com.wk.dto.CarriageDto;
import com.wk.dto.TrainDto;
import com.wk.exception.ElementAlreadyExistsException;
import com.wk.exception.ElementNotFoundException;
import com.wk.model.Carriage;
import com.wk.model.Train;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service("trainService")
public class TrainServiceImpl implements TrainService {
    @Autowired
    private TrainRepository trainRepository;
    @Autowired
    private CarriageRepository carriageRepository;

    @Transactional
    @Override
    public void init() {
        validateTrainNumber("938");
        createTrainWithCarriages("938", createCarriages("1,2"));
        validateTrainNumber("278");
        createTrainWithCarriages("278", createCarriages("3,4"));
    }
    private Collection<CarriageDto> createCarriages(String carriagesList) {
        List<CarriageDto> carriages = new ArrayList<>();
        Arrays.asList(carriagesList.split(",")).forEach(number -> carriages.add(new CarriageDto(number)));
        return carriages;
    }

    private Train createTrainWithCarriages(String trainNumber, Collection<CarriageDto> carriages) {
        Train train = trainRepository.saveAndFlush(new Train(trainNumber));
        carriages.forEach(carriage -> {
            Carriage c = carriageRepository.saveAndFlush(new Carriage(train, carriage.getCarriageNumber()));
            train.getCarriages().add(c);
        });
        return train;
    }

    @Transactional(readOnly = true)
    @Override
    public List<TrainDto> getTrains() {
        List<Train> trains = trainRepository.findAllByOneSelect();
        if(trains == null || trains.isEmpty()) {
            return null;
        }
        return convertListTrain2TrainDto(trains);
    }

    private List<TrainDto> convertListTrain2TrainDto(List<Train> trains) {
        List<TrainDto> trainDtos = new ArrayList<>();
        trains.forEach(train -> {
            Set<CarriageDto> carriageDtos = new HashSet<>();
            train.getCarriages().forEach(carriage -> carriageDtos.add(new CarriageDto(carriage.getCarriageNumber())));
            trainDtos.add(new TrainDto(train.getTrainNumber(), carriageDtos));
        });
        return trainDtos;
    }

    @Transactional(readOnly = true)
    @Override
    public TrainDto getTrain(String trainNumber) {
        Optional<Train> optionalTrain = trainRepository.findByTrainNumber(trainNumber);
        optionalTrain.orElseThrow(() -> new ElementNotFoundException(trainNumber));
        return convertTrain2Dto(optionalTrain.get());
    }

    private TrainDto convertTrain2Dto(Train train) {
        Set<CarriageDto> carriageDtos = new HashSet<>();
        train.getCarriages().forEach(carriage -> carriageDtos.add(new CarriageDto(carriage.getCarriageNumber())));
        return new TrainDto(train.getTrainNumber(), carriageDtos);
    }

    @Transactional
    @Override
    public TrainDto addTrain(TrainDto trainDto) {
        validateTrainNumber(trainDto.getTrainNumber());
        return convertTrain2Dto(createTrainWithCarriages(trainDto.getTrainNumber(), trainDto.getCarriages()));
    }

    @Transactional
    @Override
    public TrainDto updateTrainAddCarriage(String trainNumber, CarriageDto carriageDto) {
        Optional<Train> optionalTrain = trainRepository.findTrainByTrainNumber(trainNumber);
        optionalTrain.orElseThrow(() -> new ElementNotFoundException(trainNumber));
        validateCarriageNumber(trainNumber, carriageDto.getCarriageNumber());
        Carriage c = carriageRepository.saveAndFlush(new Carriage(optionalTrain.get(), carriageDto.getCarriageNumber()));
        optionalTrain.get().getCarriages().add(c);
        return convertTrain2Dto(optionalTrain.get());
    }

    @Override
    @Transactional
    public TrainDto updateTrainRemoveCarriage(String trainNumber, String carriageNumber) {
        Optional<Train> optionalTrain = trainRepository.findByTrainNumber(trainNumber);
        optionalTrain.orElseThrow(() -> new ElementNotFoundException(trainNumber));
        Optional<Carriage> optionalCarriage = trainRepository.findByTrainNumberAndCarriageNumber(trainNumber, carriageNumber);
        optionalCarriage.orElseThrow(() -> new ElementNotFoundException(carriageNumber));

        carriageRepository.delete(optionalCarriage.get());
        optionalTrain.get().getCarriages().remove(optionalCarriage.get());
        return convertTrain2Dto(optionalTrain.get());
    }

    @Override
    public void deleteAllInBatch() {
        trainRepository.deleteAllInBatch();
    }

    @Override
    public TrainDto save(Train train) {
        return convertTrain2Dto(trainRepository.save(train));
    }

    private void validateCarriageNumber(String trainNumber, String carriageNumber) {
        if(trainRepository.testCarriageNumber(trainNumber, carriageNumber)) {
            throw new ElementAlreadyExistsException(carriageNumber);
        }
    }
    private void validateTrainNumber(String trainNumber) {
        if(trainRepository.testTrainByTrainNumber(trainNumber)) {
            throw new ElementAlreadyExistsException(trainNumber);
        }
    }

}
