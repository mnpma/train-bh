package com.wk.service;

import com.wk.dao.CarriageRepository;
import com.wk.dto.CarriageDto;

import com.wk.model.Carriage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("carriageService")
public class CarriageServiceImpl implements CarriageService {
    @Autowired
    private CarriageRepository carriageRepository;

    @Override
    public CarriageDto save(Carriage carriage) {
        return new CarriageDto(carriageRepository.save(carriage).getCarriageNumber());
    }
    @Override
    public void deleteAllInBatch() {
        carriageRepository.deleteAllInBatch();
    }
}
