package com.wk.service;

import com.wk.dto.CarriageDto;
import com.wk.model.Carriage;

public interface CarriageService {
    CarriageDto save(Carriage carriage);
    void deleteAllInBatch();
}
