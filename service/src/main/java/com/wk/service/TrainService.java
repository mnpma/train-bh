package com.wk.service;

import com.wk.dto.CarriageDto;
import com.wk.dto.TrainDto;
import com.wk.model.Train;

import java.util.List;

public interface TrainService {
    void init();
    List<TrainDto> getTrains();
    TrainDto getTrain(String trainNumber);
    TrainDto addTrain(TrainDto trainDto);
    TrainDto updateTrainAddCarriage(String trainNumber, CarriageDto carriageDto);
    TrainDto updateTrainRemoveCarriage(String trainNumber, String carriageNumber);
    void deleteAllInBatch();
    TrainDto save(Train train);
}
