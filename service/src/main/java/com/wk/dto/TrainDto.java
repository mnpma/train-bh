package com.wk.dto;

import io.swagger.annotations.ApiModelProperty;
import java.util.HashSet;
import java.util.Set;

public class TrainDto {
    @ApiModelProperty(notes = "Train number (consists from digits only)", required = true)
    private String trainNumber;
    @ApiModelProperty(notes = "list of carriages")
    private Set<CarriageDto> carriages = new HashSet<>();

    public TrainDto() { }

    public TrainDto(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public TrainDto(String trainNumber, Set<CarriageDto> carriages) {
        this.trainNumber = trainNumber;
        this.carriages = carriages;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Set<CarriageDto> getCarriages() {
        return carriages;
    }

    public void setCarriages(Set<CarriageDto> carriages) {
        this.carriages = carriages;
    }
}
