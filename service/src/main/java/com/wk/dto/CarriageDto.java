package com.wk.dto;

import io.swagger.annotations.ApiModelProperty;

public class CarriageDto {
    @ApiModelProperty(notes = "Carriage number (consists from digits only)", required = true)
    private String carriageNumber;

    public CarriageDto() {
    }

    public CarriageDto(String carriageNumber) {
        this.carriageNumber = carriageNumber;
    }

    public String getCarriageNumber() {
        return carriageNumber;
    }

    public void setCarriageNumber(String carriageNumber) {
        this.carriageNumber = carriageNumber;
    }
}
