package com.wk.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ElementAlreadyExistsException extends RuntimeException {
    public ElementAlreadyExistsException(String number) {
        super("element number '" + number + "' already exist on another.");
    }
}
