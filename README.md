JSON REST API with some basic CRUD functionality

Domain modal is train.
A train consists of a number of carriages. A user of the API, can fetch trains, add trains, and add carriages to a train.
## Documentation for REST API can be seen using http://localhost:9092/swagger-ui.html when app starts...

## How to run the app
1. cd \train-bh
2. Execute command: mvnw install
3. mvnw spring-boot:run -pl rest

## The endpoints

### GET /init
Initializes the database with above data, returns 204 No Content

Test curl:
curl "http://localhost:9092/init"

### GET /train
Returns all available trains.

Return codes:
200 - ok
204 - no content

Test curl:
curl --include "http://localhost:9092/train"

### GET /train/{id}
Returns a particular train.

Return codes:
200 - ok
404 - not found

Test curl:
curl "http://localhost:9092/train/278"


### POST /train/
Creates a new train  given body and returns newly created train.
Test curl:

curl -k --include -X POST --header "Content-Type:application/json" -d '{"trainNumber":"111","carriages":[{"carriageNumber":"5"},{"carriageNumber":"6"}]}'  "http://localhost:9092/train"


Body:
javascript
{"trainNumber":"111","carriages":[{"carriageNumber":"5"},{"carriageNumber":"6"}]}


Return codes:
200 - ok
400 - train number already exist on another train

### PUT /train/{id}
modify the train by adding a carriage to it and returning the resulting train.

curl -k --include -X PUT --header "Content-Type:application/json" -d '{"carriageNumber":"7"}'  "http://localhost:9092/train/278"

Body:
javascript
{"carriageNumber":"7"}


Return codes:
200 - ok
404 - train not found
400 - bad request, for example the carriage number already exist on the particular train.

### DEL /train/{id}/carriage/{carriageId}
Remove carriage with {carriageId} from train and return the resulting train.

Return codes:
200 - ok
404 - carriage not found

Test curl:

curl --include -X DELETE "http://localhost:9092/train/278/carriage/1"


## Requirements
* The application should be able to run standalone using Maven,
* using Spring Boot (mvn spring-boot:run) or embedded Jetty (mvn jetty:run)
* Use Java 1.8
* Spring MVC, JAX-RS, or Jersey
* Use Maven or Gradle to build the project

## How to DEBUG
1. cd rest-service
2. java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8000 -jar target/rest-service-0.0.1.jar

## How to open H2 console
This sample also enables the H2 console (at http://localhost:9092/h2-console/) so that you can review the state of the database
(the default jdbc url is jdbc:h2:mem:testdb).




